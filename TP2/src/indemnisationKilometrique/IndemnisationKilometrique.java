package indemnisationKilometrique;

import java.util.Scanner;

public class IndemnisationKilometrique {
	public double distance;
	
	public IndemnisationKilometrique(double distance) { this.distance = distance; }
	
	public double indemnisation()
	{
		if(distance < 0) throw new IllegalArgumentException("Merci d'entrer une distance positive.");
		return Math.round((Math.max(Math.min(distance, 10), 	 0) * 1.50 // [  0 ; 10 ]
		 				 + Math.max(Math.min(distance, 40) - 10, 0) * 0.40 // ] 10 ; 40 ]
		 				 + Math.max(Math.min(distance, 60) - 40, 0) * 0.55 // ] 40 ; 60 ]
		 				 + Math.max((distance-60)/20, 			 0) * 6.81 // > 60
		 				 )*100)/100.0; // Arrondi au centime.
	}
	
	public static void main(String[] args)
	{
		double distance = -1 ;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Entrez votre distance :");
		
		while (distance < 0)
		{
			try { distance = Double.parseDouble(input.nextLine()); }
			catch(NumberFormatException e) { System.out.println("Merci d'entrer une distance valide."); }
		}
		
		input.close();
		
		System.out.println("Indemnité kilométrique pour un trajet de " + distance + " kilomètres : " + new IndemnisationKilometrique(distance).indemnisation() + "€.");
	}
}