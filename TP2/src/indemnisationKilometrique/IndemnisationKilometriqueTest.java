package indemnisationKilometrique;

import org.junit.Assert;
import org.junit.Test;

public class IndemnisationKilometriqueTest {
	@Test
	public void testNegative()
	{
		try {
			new IndemnisationKilometrique(-10).indemnisation();
			Assert.fail("Negative value didn't throw exception");
		} catch(IllegalArgumentException e) { }
	}
	
	@Test
	public void testNull() { Assert.assertEquals(new IndemnisationKilometrique(0).indemnisation(), 0, 0); }
	
	@Test
	public void testPositiveValues()
	{
		Assert.assertEquals(new IndemnisationKilometrique(0.1)   .indemnisation(),  0.15, 0);
		Assert.assertEquals(new IndemnisationKilometrique(17.123).indemnisation(), 17.85, 0);
		Assert.assertEquals(new IndemnisationKilometrique(39.5)  .indemnisation(), 26.80, 0);
		Assert.assertEquals(new IndemnisationKilometrique(61)    .indemnisation(), 38.34, 0);
		Assert.assertEquals(new IndemnisationKilometrique(81)    .indemnisation(), 45.15, 0);
		Assert.assertEquals(new IndemnisationKilometrique(99)    .indemnisation(), 51.28, 0);
	}
}
